import { Interpolation } from "@angular/compiler";

export interface ILoginInterface{
    username?: string;
    password?: string;
}

export class ILogin implements ILoginInterface
{
    username?: string;
    password?: string;
}
