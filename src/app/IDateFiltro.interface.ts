import { Interpolation } from "@angular/compiler";

export interface IDateFiltro{
    dt_i?: string,
    dt_f?: string
}

export class IDate implements IDateFiltro
{
    dt_i?: string;
    dt_f?: string;
}
