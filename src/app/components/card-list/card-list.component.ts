import { LoginComponent } from './../pages/login/login.component';
// import { IDadosAgrupados } from './../../IDadosUsuario.inteface';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { IDados } from 'src/app/IDados.interface';
import { IDate } from 'src/app/IDateFiltro.interface';
import { ApiService } from 'src/app/services/api.service';
import { FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.css']
})
export class CardListComponent implements OnInit {

  @Input() dados:  any[] = [];
 
  constructor( private router:Router, private api:ApiService) {}

  ngOnInit(): void {
    this.onAtivarTabela();
  }

  onAtivarTabela(){
    return this.dados;
  }

}
