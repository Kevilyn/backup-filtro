import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { IResponse } from 'src/app/IResponse.interface';
import { ILogin } from 'src/app/ILogin.interface';
import { Subscriber } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  loginForm = new FormGroup ({
    username : new FormControl('', Validators.required),
    password : new FormControl('', Validators.required)
  })

  constructor(private api:ApiService, private router:Router) { }


  errorStatus: boolean = false;
  errorMjs:any = "";

  ngOnInit() {
    this.checkSessionStorage();
  }

  checkSessionStorage(){
    if(sessionStorage.getItem('token')){
      this.router.navigate(['']);
    }
  }

  onLogin(form:any){

    let login: ILogin = {
      username: form.username,
      password: form.password
    };

    let loginInfos: any = login;

    this.api.loginByuser(login).subscribe(data =>{

      let dataResponse: IResponse = data;

      if(dataResponse.token != ""){

        sessionStorage.setItem("token", dataResponse.token);
        sessionStorage.setItem("username", loginInfos.username);

        this.router.navigate([''])

      }else{
        this.errorStatus = true;
        this.errorMjs = dataResponse.result.error_msg;
      }

    })
  }

}
