import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  userName: string | null = '';

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.userName = sessionStorage.getItem('username');
  }

  deslogar(){
    this.api.deslogar()
  }

}
