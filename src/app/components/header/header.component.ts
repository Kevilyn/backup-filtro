import { Component, OnInit } from '@angular/core';
import { SelectorMatcher } from '@angular/compiler';
import * as moment from 'moment';
import { IDados } from 'src/app/IDados.interface';
import { IDate } from 'src/app/IDateFiltro.interface';
import { ApiService } from 'src/app/services/api.service';
import { IDadosAgrupados } from 'src/app/IDadosUsuario.inteface';
import { getLocaleDayPeriods } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  usuarios: IDados[] = [];
  
  Dadinho = new FormGroup({
 registros: new FormControl,
 usuario:new FormControl,
 tipo:new FormControl,
 departamento:new FormControl
});
  constructor(private api :ApiService) { }

  ngOnInit(): void {
    let data: IDate = {

      dt_i: moment().format('MM/DD/YYYY'),
  
      dt_f: moment().format('MM/DD/YYYY')
  
    }
   this.api.getAllDado(data).subscribe(dataConvercao =>{
      this.usuarios = dataConvercao;
  //  console.log(this.usuarios);
    })
  }

}
