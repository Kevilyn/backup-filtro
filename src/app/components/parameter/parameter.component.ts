import { FormControl, FormGroup } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import * as moment from 'moment';
import { IDate } from 'src/app/IDateFiltro.interface';

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.css']
})
export class ParameterComponent implements OnInit {

  constructor (private api:ApiService, private router:Router) { }
  id : any;
  dados: any[] = [];
  selecao: any;
  @Output() ativarTabela = new EventEmitter()
  Dadinho = new FormGroup({
    id: new FormControl,
    registros: new FormControl,
    usuario:new FormControl,
    tipo:new FormControl,
    departamento:new FormControl
 });


  ngOnInit(): void {
  }

  public dateForm = new FormGroup ({
    dt_i : new FormControl(''),
    dt_f : new FormControl('')
  })

  onDate(form:any){

    let data: IDate = {
      dt_i: moment(form.dt_i).format('MM/DD/YYYY'),
      dt_f: moment(form.dt_f).format('MM/DD/YYYY')
    }
    this.api.getAllDado(data).subscribe(dataConvercao =>{
      this.dados = dataConvercao;
      console.log(this.dados);
    }) 

  }

  fitrar = 0;
 
  atualizar(){
    let idFilter;

    const result = this.dados.filter((obj) => {
      idFilter = obj.id === Number(this.selecao);
    });

    this.dados= this.dados.filter(d => d.id == Number(this.selecao))
    console.log(this.selecao);
    console.log("=================================");
    return this.selecao;
  } 

}
