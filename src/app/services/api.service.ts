import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';

import { IDados } from '../IDados.interface';
import { IDate } from '../IDateFiltro.interface';
import { IResponse } from '../IResponse.interface';
import { ILogin } from '../ILogin.interface';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient, private router: Router) { }

  loginByuser(form:ILogin):Observable<IResponse>{

    let direcction = environment.apiUrl + '/Authentication/login';
    return this.http.post<IResponse>(direcction, form);
  }

  getAllDado(page:IDate) {

    let direcction = environment.apiUrl + '/RegistrosAgrupados'
    const token = window.sessionStorage.getItem("token");

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
      'dataFinal': `${page.dt_f}`,
      'dataInicial': `${page.dt_i}`
    })

    console.log(page.dt_i);
    console.log(page.dt_f);

    return this.http.get<IDados[]>(direcction,{headers:headers});
  }

  deslogar(){
    sessionStorage.clear()
    this.router.navigate(['login'])
  }
}
